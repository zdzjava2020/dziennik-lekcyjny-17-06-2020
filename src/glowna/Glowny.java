package glowna;

import java.util.Scanner;

import organizacja.KlasaWirtualna;
import organizacja.Sala;
import osoby.Osoba;
import osoby.Uczen;
import przedmioty.MaturalneOgolne;
import przedmioty.Przedmiot;

public class Glowny {
	
	private class Menu {
		
		final String powitanie = "Witaj w dzienniku elektornicznym.";
		
		final String opcje[] = {"Nauczyciele", "Uczniowie", "Przedmioty", "Organizacja", "Koniec"};
		
	//	final String opcje = "1.Nauczyciele\n2.Uczniowie\n3.Przedmioty\n4.Koniec";
		
		final String opcje_podmenu[] = {"Dodaj","Modyfiuj","Usu�","Znajd�","Wy�wietl list�","Powr�t"};
		
//		final String opcje_podmenu = "1.Dodaj\n2.Modyfiuj\n3.Usu�\n4.Znajd�\n5.Wy�wietl list�\n6.Powr�t";
		
		final String stopka = "\nTw�j wyb�r: ";
		
		int aktualneMenu;
		
		Scanner wejscie;
		
		//0 - menu g�owne, 1 - podmenu
		private String budujMenu(int opcja) {
			String m[] = (opcja==0) ? opcje : opcje_podmenu;
			String ret = "";
			for (int i=0;i<m.length;i++) {
				ret+= (i+1) + "." + m[i] + "\n";
			}
			return ret;
		}
		
		public Menu() {
			aktualneMenu=0;
			wejscie=new Scanner(System.in);
		}
		
		public Menu(Scanner s) {
			aktualneMenu=0;
			wejscie=s;
		}
		
		public void start() {
			System.out.println(powitanie);
			for(;;) {
				if (aktualneMenu==0) 
					System.out.print(budujMenu(0));
				else if (aktualneMenu==opcje.length)
					break;
				else 
					System.out.print(budujMenu(1));
				System.out.print(stopka);
				int wybor = wejscie.nextInt();
				if (wybor==opcje_podmenu.length) {
					aktualneMenu=0;
					continue;
				}
				switch(aktualneMenu) {
					case 0: aktualneMenu=wybor; break;
					case 1: /*tutaj przej�cie od os�b-nauczycieli*/ break;
					case 2:  uczniowie(); break;
					case 3:   przedmioty(); break;
					case 4: klasy(); break;
				}
				if (aktualneMenu==0) aktualneMenu=wybor;
				
			}
			System.out.println("Do zobaczenia!");
		}
		
		private void klasy() {
			KlasaWirtualna kw = new KlasaWirtualna();
			kw.addUczestnik(new Osoba());
			kw.getUczestnik();
		}
		
//		private void przedmioty() {
//			Przedmiot p = new Przedmiot();
//			p.setPoziom();
//			p.setTematy("Zasady BHP i oceniania", 1);
//			p.setTematy("Wst�p do elektroniki", 1);
//			p.setTematy("Systemy liczbowe", 1);
//			
//			//tablica dwuwymiarowa redukuje si� do jednowymiarowej gdy� foreach kolejno zdjemuje do zmiennej t
//			//zawarto�� wskazanej (kolejnej) tablicy z tematami
//			for (String[] t : p.getTematy()) {
//				//ten foreach pozyskuje dla nas pojedyncze tematy zapisane w danym roku szkolnym
//				if (t==null) continue;
//				for(String tt: t) {
//					System.out.println(tt);
//				}
//			}
//		}
		
		private void przedmioty() {
			MaturalneOgolne p = new MaturalneOgolne(wejscie);
			
			System.out.println("WYJ�CIE W METODZIE G��WNEJ");
			System.out.println(p.getNazwa());
		
			p.setPoziom();
			p.setTematy("Zasady BHP i oceniania", 1);
			p.setTematy("Wst�p do elektroniki", 1);
			p.setTematy("Systemy liczbowe", 1);
			
			//tablica dwuwymiarowa redukuje si� do jednowymiarowej gdy� foreach kolejno zdjemuje do zmiennej t
			//zawarto�� wskazanej (kolejnej) tablicy z tematami
			for (String[] t : p.getTematy()) {
				//ten foreach pozyskuje dla nas pojedyncze tematy zapisane w danym roku szkolnym
				if (t==null) continue;
				for(String tt: t) {
					System.out.println(tt);
				}
			}
			
			Przedmiot matura = new MaturalneOgolne();
			matura.setPoziom();
			//matura.getZ
		}
		
		private void uczniowie() {
			Uczen uczen = new Uczen(wejscie);
			uczen.dodajDane();
			uczen.wyswietlDane();
		}
		
	} //koniec definicji klasy Menu
	//zmienna wejscie w klasie Menu nie ma NIC wsp�lnego ze zmienn� zainicjalizowan� w funkcji main()
	
	void menu(Scanner s) {
		new Menu(s).start();
	}
	
	public static void main(String[] args) {
		new Sala();
		
		Scanner wejscie = new Scanner(System.in);
		new Glowny().menu(wejscie);
		//new Menu(wejscie).start();
		wejscie.close();
	}
}
