package przedmioty;

import java.util.Scanner;

public class MaturalneOgolne extends Przedmiot {
	
	private String nazwa;
	
	private String zagadnienia[];
	
	public MaturalneOgolne() {
		super();
		
	}
	
	public MaturalneOgolne(Scanner s) {
		super(s);
		nazwa="J�zyk polski";
		super.setNazwa("Przedmiot generyczny");
		System.out.println("Nazwa przedmiotu: " + getNazwa() + ", za� nazwa przedmiotu z klasy przedmiot: " + super.getNazwa());
	}
	

	
	public void setNazwa(String s) {
		nazwa= s;
	}
	
	@Override
	public String getNazwa() {
		return nazwa;
	}

	@Override
	public void setPoziom() {
		// TODO Auto-generated method stub
		setPoziom(1, 4);
	}
	
	public String[] getZagadnieniaMatura() {
		return zagadnienia;
	}
	
//	 public void abstract setPoziom() {
//			int tmp=1;
//			System.out.print("Podaj klas� pocz�tkow� dla przedmiotu:");
//			tmp=wejscie.nextInt();
//			System.out.print("Podaj ilo�� lat kontynuowania przedmiotu:");
//			setPoziom(tmp, wejscie.nextInt());
//		}
}
