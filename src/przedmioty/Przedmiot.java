package przedmioty;

import java.util.Arrays;
import java.util.Scanner;

abstract public class Przedmiot {
	
	private String nazwa,opis;
	private int grupa;
	private int poziom; //klasa, w kt�ej jest zaczyna by� nauczany
	private int semestr; //start semestru nauczania
	private String tematy[][];
	private boolean praktyczny;
	
	private int kategoria;
	
	private Scanner wejscie;
	
	public Przedmiot() {
		wejscie=new Scanner(System.in);
	}
	
	public Przedmiot(Scanner s) {
		wejscie=s;
	}
	
	public int wezLiczbe() {
		try {
			return wejscie.nextInt();
		}
		catch (Exception e) {
			wejscie.next(); 
			return 0;
		}
	}
	
	public String wezCiag() {
		return wejscie.nextLine();
	}
	
	public void setNazwa(String s) {
		nazwa=s;
	}
	
	public String getNazwa() {
		return nazwa;
	}
	
	public void setOpis(String s) {
		opis=s;
	}
	
	public String getOpis() {
		return opis;
	}
	
	public void setGrupa(int i) {
		grupa=i;
	}
	
	public int getGrupa() {
		return grupa;
	}
	
	
	/* 
	 * to jest przyk�ad metody wirtualnej (abstrakcyjnej). Oznacza to, �e nie b�dzie ona posiada� w tym miejscu
	 * swojej definicji. Zamiast tego ka�da klasa dziedzicz�ca po tej klase b�dzie musia�a posiada� definicj�
	 * tej�e metody. To ujednolici stuktur� wewn�trzn� wszystkich obiekt�w. Natomiast jej dzia�anie
	 * mo�e si� znacz�co r�ni� pomi�dzy poszczeg�lnymi klasami (i obiektami z nich tworzonymi). 
	 */
	abstract public void setPoziom();
	
	public void setPoziom(int i) {
		setPoziom(i, 1);
	}
	
	public void setPoziom(int i, int j) {
		poziom=i;
		tematy = new String[j][];
	}
	
	
	
	public int getPoziom() {
		return poziom;
	}
	
	public void setSemestr(int i) {
		semestr=i;
	}
	
	public int getSemestr() {
		return semestr;
	}
	
	public void setTematy(String t[][]) {
		tematy=t;
	}
	
	public void setTematy(String t[], int poziom) {
		if (this.poziom > poziom) {
			//bo klasa podana jest m�odsza ni� start przedmiotu!
			return;
		}
		/*
		 * this.poziom -> poziom startu przedmiotu (np. 2 znaczy w drugiej klasie); podczas tworzenia przedmiotu
		 * tablica powinna dosta� tak�e informacj� ile dany przedmiot b�dzie uczony. 
		 * 
		 * oznacza to, �e pierwszy indeks 0 w tablicy dwuwymiarowej tematy b�dzie odnosi� si� realnie do poziomu 2 nauki
		 * je�eli przedmio b�dzie nauczany dalej - to tablica za ka�dym razem powinna by� zwi�kszana do odpowiedniej warto�ci
		 * chyba, �e ju� posiada dany poziom. 
		 */
		if (tematy[poziom-this.poziom] == null) {
			//tablica ju� zawiera odpowiedni� ilo�� 
			tematy=Arrays.copyOf(tematy, (poziom-this.poziom)+1);
		}
		tematy[poziom-this.poziom]=t;
	}
	
	public void setTematy(String t, int poziom) {
		if (this.poziom > poziom) {
			return;
		}
		if (tematy.length <= poziom-this.poziom) {
			tematy=Arrays.copyOf(tematy, (poziom-this.poziom)+1);
		}
		
//		if (tematy[poziom-this.poziom] == null) {
//			tematy=Arrays.copyOf(tematy, (poziom-this.poziom)+1);
//			
//		}
		if (tematy[poziom-this.poziom] == null) {
			tematy[poziom-this.poziom] = new String[1];
		}
		else
			tematy[poziom-this.poziom] = Arrays.copyOf(tematy[poziom-this.poziom], tematy[poziom-this.poziom].length+1);
		tematy[poziom-this.poziom][tematy[poziom-this.poziom].length-1]=t;
	}
	
	public String[][] getTematy() {
		return tematy;
	}
	
	public String[] getPoziomTematy(int poziom) {
		/*
		 * Je�eli przyk�adowo b�dziemy chcieli pobra� tematy z klasy 3 (poziom 3) a przedmiot zacz�� si� w klasie
		 * 2 to wypada�oby, by nasza tablica2D z tematami mia�a d�ugo�� 2 (2 elementy). Je�eli tak, to warunek
		 * zwrotny powinien wynosi� (3-2) -> 1 
		 * CZYLI tematy zwi�zane z klas� 3 b�d� pod indeksem 1; je�eli tablica b�dzie mia�a mniejsz� b�d� r�wn� d�ugo��
		 * oznaczacza� to b�dzie, �e temat�w z klasy 3 NIKT NIE DODA�. WI�c albo zwracamy tematy dla naszej klasy
		 *  albo warto�� null (brak jakichkolwiek temat�w)
		 */
		return (tematy.length > poziom-this.poziom) ? tematy[poziom-this.poziom] : null;
	}
	
	
	
	public void setPraktyczny(boolean b) {
		praktyczny=b;
	}
	
	public boolean getPrkatyczny() {
		return praktyczny;
	
	}
}
