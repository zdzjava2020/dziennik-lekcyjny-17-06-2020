package organizacja;

import osoby.*;
import przedmioty.Przedmiot;

public interface Grupa {
//	public Nauczyciel opiekun[] = null;
//	public Osoba uczestnik[] = null;
//	static Przedmiot zagadnienie = null;
//	
//	public Sala pracownia = null;
	
	
	public void addOpiekun(Nauczyciel opiekun);
	
	public void addUczestnik(Osoba uczestnik);
	
	public void setZagadnienie(Przedmiot zagadnienie);
	
	public void setPracownia(Sala pracownia);
	
	public default Osoba getUczestnik() {
		System.out.println("JESTEM GENERYCZNA!");
		return null;
	}
	
	public Osoba getUczestnik(int index);
	
	public Osoba getUczestnik(String szukaj);	
	
}
