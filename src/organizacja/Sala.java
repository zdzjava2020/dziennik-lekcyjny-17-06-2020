package organizacja;

public class Sala {
	String numer;
	double pietro;
	
	public Sala() {
		this("1",0.);
	}
	
	
	
	public Sala(String numer, int pietro) {
		this(numer, pietro*1.);
	}
	
	public Sala(String numer, String pietro) {
		this(numer, Double.valueOf(pietro));
	}
	
	public Sala(String numer, double pietro) {
		this.numer=numer;
		this.pietro=pietro;
	}
	
	public String getNumer() {
		return numer;
	}
	
	public double getPietro() {
		return pietro;
	}
}
