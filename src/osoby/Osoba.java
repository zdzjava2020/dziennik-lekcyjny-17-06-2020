package osoby;

import java.util.Scanner;

public class Osoba {
	private String imie, nazwisko;
	private String ulica,numer,mieszkanie,miasto,kod;
	private int typ_ulica; //0 - ulica, 1 - aleja, 2 - plac
	private String pesel, data_urodzenia;
	private String telefon, email;
	
	protected Scanner wejscie;
	
	final private String dozwoloneZnaki="����󜿟";
	
	private String sprawdzCiag(String s) throws Exception {
		return 	sprawdzCiag(s, "", false, true);	
	}
	
	private String sprawdzCiag(String s, boolean liczby) throws Exception {
		return 	sprawdzCiag(s, "", liczby, false);	
	}
	
	private String sprawdzCiag(String s, boolean liczby, boolean znaki) throws Exception {
		return 	sprawdzCiag(s, "", liczby, znaki);	
	}
	
	private String sprawdzCiag(String s, String dozwoloneZNaki) throws Exception {
		return 	sprawdzCiag(s, dozwoloneZNaki, false, true);	
	}
	
	private String sprawdzCiag(String s, String dozwoloneZNaki, boolean liczby) throws Exception {
		return 	sprawdzCiag(s, dozwoloneZNaki, liczby, true);	
	}
	
	private String sprawdzCiag(String s, boolean liczby, String dozwoloneZNaki) throws Exception {
		return 	sprawdzCiag(s, dozwoloneZNaki, liczby, true);	
	}
	
	private String sprawdzCiag(String s, boolean liczby, boolean znaki, String dozwoloneZNaki) throws Exception {
		return 	sprawdzCiag(s, dozwoloneZNaki, liczby, znaki);	
	}
	
	
	
	private String sprawdzCiag(String s, String dozwoloneZnaki, boolean liczby, boolean znaki) throws Exception {
		dozwoloneZnaki+=this.dozwoloneZnaki;
		s=s.toLowerCase();
		for (char z : s.toCharArray()) {
			if (z >= '0' && z <= '9' && !liczby)
				throw new Exception("Podany ci�g " + s + " nie mo�e zawiera� cyfr!");
			//je�eli dowolny znak b�dzie spoza zakresu a i z ORAZ warto�� zmiennej znaki b�dzie true 
			//ORAZ nie b�dzie nale�a� do liczy dozwolonych znak�w
			else if (!(z >= 'a' && z <= 'z' ) && !dozwoloneZnaki.contains(String.valueOf(z)) && znaki)
				throw new Exception("Podany ci�g " + s + " nie mo�e zawiera� niedowolonych znak�w! (" + z + ")");
		}	
		return String.valueOf(s.charAt(0)).toUpperCase() + s.substring(1);
	}
	
	public void setImie() {
		System.out.print("Podaj swoje imi�: ");
		setImie(wejscie.nextLine());
	}
		
	
	public void setImie(String s) {
		try {
			imie=sprawdzCiag(s);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String getImie() {
		return imie;
	}
	
	public void setNazwisko() {
		System.out.print("Podaj swoje nazwisko: ");
		setNazwisko(wejscie.nextLine());
	}
	
	public void setNazwisko(String s) {

		try {
			nazwisko=sprawdzCiag(s);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String getNazwisko() {
		return nazwisko;
	}
	
	
	public void setUlica(String s) {

		try {
			ulica=sprawdzCiag(s, " -", true);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String getUlica() {
		return ulica;
	}
	
	public void setMiasto(String s) {

		try {
			miasto=sprawdzCiag(s, true, " -");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String getMiasto() {
		return miasto;
	}
	
	public void setKodPocztowy(String s) {
		if ((s.contains("-") && s.length()!=6) ||
				(!s.contains("-") && s.length()!=5)) {
			System.out.println("Podany ci�g znakowy nie jest kodem pocztowym");
			return;
		}
		try {
			kod=sprawdzCiag(s, "", true, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getKodPocztowy() {
		return kod;
	}
		
	public void setNumer(String s) {
		try {
			numer=sprawdzCiag(s, "/", true, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getNumer() {
		return numer;
	}
	
	public void setMieszkanie(String s) {
		try {
			mieszkanie=sprawdzCiag(s, "/", true, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getMieszkanie() {
		return mieszkanie;
	}
	
	public void setPESEL() {
		System.out.print("Podaj PESEL: ");
		setPESEL(wejscie.nextLine());
		
	}
	
	public void setPESEL(String s) {
		if (s.length()!=11) {
			System.out.println("Nie poda�e� poprawnego numeru PESEL");
			return;
		}
		try {
			pesel=sprawdzCiag(s, true, false);
			
			String rok = pesel.substring(0,2);
			//String miesiac = pesel.substring(2,4);
			int miesiac = Integer.valueOf(pesel.substring(2,4));//Integer.valueOf(String.valueOf(pesel.charAt(2) + pesel.charAt(3)));
			String dzien = pesel.substring(4,6);
			
			//ludzie urodzeni przez 1900 rokiem
			//miesi�c stycze�-wrzesie�: 81 82 83 84 85 86 87 88 89 (na miejscu dziesi�tek mamy 8)
			//miesi�c pa�dziernik-grudzie�: 90 91 92 (na miejscu dziesi�tek mamy 9)
			
			//ludzie urodzeni pomi�dzu 1900-1999
			//miesi�c stycze�-wrzesie�: 01 02 03 04 05 06... 09
			//miesi�c pa�dziernik-grudzie�: 10 11 12
			
			//ludzie urodzeni 2000 -> dalej
			//miesi�c stycze�-wrzesie�: 21 22 23 24 25 26... 29
			//miesi�c pa�dziernik-grudzie�: 30 31 32
			
			if (Integer.valueOf(miesiac) > 20) {
				rok="20" + rok;
				miesiac-=20; //String.valueOf(Integer.valueOf(miesiac)-20);
				//if (miesiac.length()==1) miesiac="0"+miesiac;
			}
			else
				rok="19" + rok;
				
			data_urodzenia = dzien + "-" + ((miesiac<10) ? "0" : "") + miesiac + "-" + rok;
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getPESEL() {
		return pesel;
	}
	
	public String getDataUrodzenia() {
		return data_urodzenia;
	}
	
}
