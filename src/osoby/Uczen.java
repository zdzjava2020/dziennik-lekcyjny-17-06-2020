package osoby;

import java.util.Scanner;

public class Uczen extends Osoba{
	private String kierunek;
	private int typ_szkoly; //0 - podstawowa, 1 - liceum, 2 - technikum, 3 - zawodowa, 4 - medyczna, 5 - policealna
	private String legitymacja; // numer legitymacji
	
	public Uczen() {
		wejscie = new Scanner(System.in);
	}
	
	public Uczen(Scanner s) {
		wejscie = s;
	}
	
	public void dodajDane() {
		setImie();
		setNazwisko();
		setPESEL();
	}
	
	public void wyswietlDane() {
		System.out.println("Dane ucznia: ");
		System.out.println(getImie());
		System.out.println(getNazwisko());
		System.out.println(getPESEL());
		System.out.println(getDataUrodzenia());
	}
}
