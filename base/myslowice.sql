-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Cze 2020, 21:37
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `myslowice`
--
CREATE DATABASE IF NOT EXISTS `myslowice` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `myslowice`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cyklenauki`
--

DROP TABLE IF EXISTS `cyklenauki`;
CREATE TABLE `cyklenauki` (
  `id_cyklnauki` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL COMMENT 'przyjazna nazwa cyklu nauczania',
  `czas_trwania` tinyint(2) UNSIGNED NOT NULL COMMENT 'w latach'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `cyklenauki`
--

INSERT INTO `cyklenauki` (`id_cyklnauki`, `nazwa`, `czas_trwania`) VALUES
(1, 'Technikum młodzieżowe', 4),
(2, 'Technikum młodzieżowe', 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dziedziny`
--

DROP TABLE IF EXISTS `dziedziny`;
CREATE TABLE `dziedziny` (
  `id_dziedzina` tinyint(2) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `dziedziny`
--

INSERT INTO `dziedziny` (`id_dziedzina`, `nazwa`) VALUES
(1, 'humanistyczna'),
(2, 'ścisła'),
(3, 'zawodowa'),
(4, 'medyczna'),
(5, 'ścisłoteoretyczna');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasy`
--

DROP TABLE IF EXISTS `klasy`;
CREATE TABLE `klasy` (
  `id_klasa` bigint(20) UNSIGNED NOT NULL,
  `id_cyklnauki` bigint(20) UNSIGNED NOT NULL,
  `profil` varchar(50) NOT NULL,
  `rok_rozpoczecia` year(4) NOT NULL,
  `identyfikator` varchar(10) NOT NULL,
  `rok_zakonczenia` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `klasy`
--

INSERT INTO `klasy` (`id_klasa`, `id_cyklnauki`, `profil`, `rok_rozpoczecia`, `identyfikator`, `rok_zakonczenia`) VALUES
(1, 1, 'TECHNIK INFORMATYK', 2019, 'AG', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `nauczyciele`
--

DROP TABLE IF EXISTS `nauczyciele`;
CREATE TABLE `nauczyciele` (
  `id_nauczyciel` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `id_dziedzina` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `nauczyciele`
--

INSERT INTO `nauczyciele` (`id_nauczyciel`, `id_osoba`, `id_dziedzina`) VALUES
(1, 11, 3),
(2, 10, 3),
(3, 8, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `nauczyciele_klasy`
--

DROP TABLE IF EXISTS `nauczyciele_klasy`;
CREATE TABLE `nauczyciele_klasy` (
  `id_nauczyciel` bigint(20) UNSIGNED NOT NULL,
  `id_klasa` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `nauczyciele_przedmioty`
--

DROP TABLE IF EXISTS `nauczyciele_przedmioty`;
CREATE TABLE `nauczyciele_przedmioty` (
  `id_nauczyciel` bigint(20) UNSIGNED NOT NULL,
  `id_przedmiot` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(40) NOT NULL,
  `nazwisko` varchar(70) NOT NULL,
  `ulica` varchar(50) NOT NULL,
  `numer` varchar(10) NOT NULL,
  `mieszkanie` varchar(10) DEFAULT NULL,
  `miasto` varchar(60) NOT NULL,
  `kod` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `osoby`
--

INSERT INTO `osoby` (`id_osoba`, `imie`, `nazwisko`, `ulica`, `numer`, `mieszkanie`, `miasto`, `kod`) VALUES
(1, 'Joanna', 'Brzezińska', 'Motyla', '8', NULL, 'Łódź', '90-019'),
(2, 'Marcin', 'Suchecki', 'Łąkowa', '89', '20', 'Kostrzyń', '22-200'),
(3, 'Julita', 'Freeman', 'Żwirki i Wigury', '178a', NULL, 'Warszawa', '00-918'),
(4, 'Oskar', 'Ligęza', 'Maratończyków', '13/12', NULL, 'Jarocin', '22-178'),
(5, 'Monika', 'Lichota', 'Lotników', '9/12a', NULL, 'Olsztyn', '98-100'),
(6, 'Natalia', 'Wrońska', '3-go maja', '15', NULL, 'Olkusz', '38-700'),
(7, 'Jadwiga', 'Wrońska', '3-go maja', '15', NULL, 'Olkusz', '38-700'),
(8, 'Natalia', 'Wrońska', '3-go maja', '15', NULL, 'Warszawa', '00-890'),
(9, 'Natalia', 'Pawlik', '3-go maja', '15', NULL, 'Olkusz', '38-700'),
(10, 'Kasjan', 'Adamski', 'Łąkowa', '167', NULL, 'Poznań', '60-700'),
(11, 'Adam', 'Koluszkowski', 'Górzysta', '12a', NULL, 'Kraków', '30-120'),
(12, 'Justyna', 'Iksińska', 'Spokojna', '12', '5a', 'Gliwice', '42-670'),
(13, 'Justyna', 'Iksińska', 'Spokojna', '12', '5a', 'Gliwice', '42-670'),
(14, 'Justyna', 'Iksińska', 'Spokojna', '12', '5a', 'Gliwice', '42-670'),
(15, 'Justyna', 'Iksińska', 'Spokojna', '12', '5a', 'Gliwice', '42-670');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmioty`
--

DROP TABLE IF EXISTS `przedmioty`;
CREATE TABLE `przedmioty` (
  `id_przedmiot` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `opis` varchar(1000) NOT NULL DEFAULT '',
  `nazwa_skrocona` varchar(10) NOT NULL,
  `id_dziedzina` tinyint(2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przedmioty`
--

INSERT INTO `przedmioty` (`id_przedmiot`, `nazwa`, `opis`, `nazwa_skrocona`, `id_dziedzina`) VALUES
(1, 'Język polski', '', '', NULL),
(2, 'Matematyka', '', '', NULL),
(3, 'Fizyka', '', '', NULL),
(4, 'Wychowanie fizyczne', '', '', NULL),
(5, 'geografia', 'opis przedmiotu', 'geo', 1),
(6, 'historia', 'opis przedmiotu', 'hist', 1),
(7, 'chemia', '', 'chm', NULL),
(8, 'fizyka', '', 'fiz', NULL),
(11, 'chemia', '', 'chm', NULL),
(12, 'informatyka', '', 'inf', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczniowie`
--

DROP TABLE IF EXISTS `uczniowie`;
CREATE TABLE `uczniowie` (
  `id_uczen` bigint(20) UNSIGNED NOT NULL,
  `kierunek` varchar(40) NOT NULL,
  `typ_szkoly` tinyint(2) NOT NULL,
  `legitymacja` varchar(10) NOT NULL COMMENT 'Numer legitymacji',
  `id_osoba` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `uczniowie`
--

INSERT INTO `uczniowie` (`id_uczen`, `kierunek`, `typ_szkoly`, `legitymacja`, `id_osoba`) VALUES
(1, 'Technik informatyk', 0, '987678', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczniowie_klasy`
--

DROP TABLE IF EXISTS `uczniowie_klasy`;
CREATE TABLE `uczniowie_klasy` (
  `id_uczen` bigint(20) UNSIGNED NOT NULL,
  `id_klasa` bigint(20) UNSIGNED NOT NULL,
  `rok_rozpoczecia` year(4) DEFAULT NULL,
  `rok_zakonczenia` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `uczniowie_klasy`
--

INSERT INTO `uczniowie_klasy` (`id_uczen`, `id_klasa`, `rok_rozpoczecia`, `rok_zakonczenia`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `widok_nauczyciele_dziedziny`
-- (Zobacz poniżej rzeczywisty widok)
--
DROP VIEW IF EXISTS `widok_nauczyciele_dziedziny`;
CREATE TABLE `widok_nauczyciele_dziedziny` (
`Imię i nazwisko nauczyciela` varchar(111)
,`Nazwa dziedziny` varchar(50)
);

-- --------------------------------------------------------

--
-- Struktura widoku `widok_nauczyciele_dziedziny`
--
DROP TABLE IF EXISTS `widok_nauczyciele_dziedziny`;

DROP VIEW IF EXISTS `widok_nauczyciele_dziedziny`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `widok_nauczyciele_dziedziny`  AS  select concat(`osoby`.`imie`,' ',`osoby`.`nazwisko`) AS `Imię i nazwisko nauczyciela`,`dziedziny`.`nazwa` AS `Nazwa dziedziny` from ((`osoby` join `nauczyciele` `n`) join `dziedziny`) where `dziedziny`.`id_dziedzina` = `n`.`id_dziedzina` and `osoby`.`id_osoba` = `n`.`id_osoba` ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `cyklenauki`
--
ALTER TABLE `cyklenauki`
  ADD UNIQUE KEY `id_cyklnauki` (`id_cyklnauki`);

--
-- Indeksy dla tabeli `dziedziny`
--
ALTER TABLE `dziedziny`
  ADD PRIMARY KEY (`id_dziedzina`);

--
-- Indeksy dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD UNIQUE KEY `id_klasa` (`id_klasa`),
  ADD KEY `cykl` (`id_cyklnauki`);

--
-- Indeksy dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  ADD UNIQUE KEY `id_nauczyciel` (`id_nauczyciel`),
  ADD KEY `id_osoba` (`id_osoba`),
  ADD KEY `dziedzina` (`id_dziedzina`);

--
-- Indeksy dla tabeli `nauczyciele_klasy`
--
ALTER TABLE `nauczyciele_klasy`
  ADD KEY `id_nauczyciel` (`id_nauczyciel`),
  ADD KEY `id_klasa` (`id_klasa`);

--
-- Indeksy dla tabeli `nauczyciele_przedmioty`
--
ALTER TABLE `nauczyciele_przedmioty`
  ADD KEY `id_nauczyciel` (`id_nauczyciel`),
  ADD KEY `id_przedmiot` (`id_przedmiot`);

--
-- Indeksy dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `id_osoba` (`id_osoba`);

--
-- Indeksy dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  ADD UNIQUE KEY `id_przedmiot` (`id_przedmiot`),
  ADD KEY `id_dziedzina` (`id_dziedzina`);

--
-- Indeksy dla tabeli `uczniowie`
--
ALTER TABLE `uczniowie`
  ADD UNIQUE KEY `id_uczen` (`id_uczen`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indeksy dla tabeli `uczniowie_klasy`
--
ALTER TABLE `uczniowie_klasy`
  ADD KEY `id_uczen` (`id_uczen`),
  ADD KEY `id_klasa` (`id_klasa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `cyklenauki`
--
ALTER TABLE `cyklenauki`
  MODIFY `id_cyklnauki` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `dziedziny`
--
ALTER TABLE `dziedziny`
  MODIFY `id_dziedzina` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `klasy`
--
ALTER TABLE `klasy`
  MODIFY `id_klasa` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  MODIFY `id_nauczyciel` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  MODIFY `id_przedmiot` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT dla tabeli `uczniowie`
--
ALTER TABLE `uczniowie`
  MODIFY `id_uczen` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD CONSTRAINT `klasy_ibfk_3` FOREIGN KEY (`id_cyklnauki`) REFERENCES `cyklenauki` (`id_cyklnauki`);

--
-- Ograniczenia dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  ADD CONSTRAINT `nauczyciele_ibfk_1` FOREIGN KEY (`id_dziedzina`) REFERENCES `dziedziny` (`id_dziedzina`),
  ADD CONSTRAINT `nauczyciele_ibfk_2` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `nauczyciele_klasy`
--
ALTER TABLE `nauczyciele_klasy`
  ADD CONSTRAINT `nauczyciele_klasy_ibfk_1` FOREIGN KEY (`id_klasa`) REFERENCES `klasy` (`id_klasa`),
  ADD CONSTRAINT `nauczyciele_klasy_ibfk_2` FOREIGN KEY (`id_nauczyciel`) REFERENCES `nauczyciele` (`id_nauczyciel`);

--
-- Ograniczenia dla tabeli `nauczyciele_przedmioty`
--
ALTER TABLE `nauczyciele_przedmioty`
  ADD CONSTRAINT `nauczyciele_przedmioty_ibfk_1` FOREIGN KEY (`id_nauczyciel`) REFERENCES `nauczyciele` (`id_nauczyciel`),
  ADD CONSTRAINT `nauczyciele_przedmioty_ibfk_2` FOREIGN KEY (`id_przedmiot`) REFERENCES `przedmioty` (`id_przedmiot`);

--
-- Ograniczenia dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  ADD CONSTRAINT `przedmioty_ibfk_1` FOREIGN KEY (`id_dziedzina`) REFERENCES `dziedziny` (`id_dziedzina`);

--
-- Ograniczenia dla tabeli `uczniowie`
--
ALTER TABLE `uczniowie`
  ADD CONSTRAINT `uczniowie_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `uczniowie_klasy`
--
ALTER TABLE `uczniowie_klasy`
  ADD CONSTRAINT `uczniowie_klasy_ibfk_1` FOREIGN KEY (`id_klasa`) REFERENCES `klasy` (`id_klasa`),
  ADD CONSTRAINT `uczniowie_klasy_ibfk_2` FOREIGN KEY (`id_uczen`) REFERENCES `uczniowie` (`id_uczen`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
